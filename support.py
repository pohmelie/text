import time
import pathlib
import pickle
import os

import tqdm


class Stepper:

    def __init__(self):
        self.steps = []

    def step(self, *, cache=True, **kw):

        def decorator(f):
            self.steps.append(dict(f=f, cache=cache, kw=kw))
            return f

        return decorator

    def run(self, source=None, *, unchanged=False, start_index=None):
        global_start = time.perf_counter()
        precalculated_source_path = None
        start_path = pathlib.Path().absolute()

        def progress(iterable):
            nonlocal progress_used
            progress_used = True
            print()
            return tqdm.tqdm(iterable)

        for i, step in enumerate(self.steps, start=1):
            if start_index > i:
                continue
            progress_used = False
            step_name = f"{i:0>2} - {step['f'].__name__}"
            pathlib.Path(step_name).mkdir(parents=True, exist_ok=True)
            os.chdir(step_name)
            path = pathlib.Path(f"{step_name}.result")
            start = time.perf_counter()
            cached = unchanged and path.exists() and step["cache"]
            cached_str = " [cached]" if cached else ""
            print(f"{i:0>2}/{len(self.steps)}{cached_str} {step['f'].__name__}", end="", flush=True)
            if cached:
                precalculated_source_path = path.absolute()
            else:
                unchanged = False
                if precalculated_source_path is not None:
                    with precalculated_source_path.open(mode="rb") as f:
                        source = pickle.load(f)
                    precalculated_source_path = None
                source = step["f"](source, progress=progress, **step["kw"])
            if not unchanged and step["cache"]:
                with path.open(mode="wb") as f:
                    pickle.dump(source, f)
            if not progress_used:
                print(f" took {time.perf_counter() - start:.3f}")
            os.chdir(start_path)
        print(f"Total: {time.perf_counter() - global_start:.3f}")
        return source
