import string
import operator
import collections
import pathlib
import itertools
import contextlib
import json
import csv

from support import Stepper


stepper = Stepper()


@stepper.step(filename="Полная парадигма.  Морфология.txt")
def read_file(*_, filename, progress=lambda x: x):
    lines = []
    with open(filename, encoding="cp1251") as f:
        for line in progress(f):
            lines.append(line.rstrip())
    return lines


@stepper.step()
def step_1_strip_tail_numbers(lines, progress):
    stripped_lines = []
    for line in progress(lines):
        stripped_lines.append(line.rstrip(string.digits).rstrip())
    return stripped_lines


@stepper.step()
def step_2_unlatinify(lines, progress):
    allowed_chars = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя \t-*0123456789/,"
    allowed = set(allowed_chars) | set(allowed_chars.upper())
    t = {ord("c"): ord("с")}
    unlatin_lines = []
    for line in progress(map(operator.methodcaller("translate", t), lines)):
        sline = set(line)
        if sline - allowed:
            print("\n", set(line) - allowed, line)
            raise Exception("Unknown char")
        unlatin_lines.append(line)
    return unlatin_lines


def gen_groups(lines, separator="\t"):
    group = []
    for line in map(str.strip, lines):
        if not line:
            if group:
                yield group
                group = []
        else:
            group.append(list(map(str.strip, line.split(separator))))
    if group:
        yield group


@stepper.step()
def step_3_cut_combination_of_words(lines, progress):
    passed_groups = []
    cutted_groups = []
    whitespaces = set(string.whitespace)
    for group in progress(gen_groups(lines)):
        if whitespaces & set(group[0][0]):
            cutted_groups.append(group)
        else:
            passed_groups.append(group)
    store_groups(cutted_groups, filename="Словосочетания.txt")
    return passed_groups


@stepper.step()
def step_4_cut_single_groups(groups, progress):
    passed_groups = []
    cutted_groups = []
    for group in progress(groups):
        if len(group) == 1:
            cutted_groups.append(group)
        else:
            passed_groups.append(group)
    store_groups(cutted_groups, filename="Одиночки.txt")
    return passed_groups


@stepper.step()
def step_5_cut_stars(groups, progress):
    passed_groups = []
    cutted_groups = []
    for group in progress(groups):
        if all(words[0].startswith("*") for words in group):
            cutted_groups.append(group)
        else:
            passed_groups.append(group)
    store_groups(cutted_groups, filename="Звёздочки.txt")
    return passed_groups


@stepper.step()
def groups_to_lines(groups, progress=lambda x: x):
    lines = []
    for group in progress(groups):
        if lines:
            lines.append("")
        for words in group:
            lines.append("\t".join(words))
    return lines


@stepper.step(filename="Полная парадигма.  Морфология.changed.txt")
def write_file(lines, *, filename, progress=lambda x: x):
    with open(filename, "w", encoding="cp1251", newline="\r\n") as f:
        for line in progress(lines):
            f.write(line)
            f.write("\n")


stepper.step(filename="Рабочий-pre-06.txt")(read_file)


@stepper.step()
def step_6_1_cut_left_stars(lines, progress):
    heads = set(read_file(filename="6 - Список 1.txt"))
    passed_groups = []
    removed = []
    for group in gen_groups(lines):
        if group[0][0].lstrip("*") in heads:
            removed.append(group)
        else:
            passed_groups.append(group)
    store_groups(removed, filename="Кунсткамера.txt")
    return passed_groups


@stepper.step()
def step_6_2_cut_left_stars(groups, progress):
    heads = set(read_file(filename="6 - Список 2.txt"))
    removed_groups = []
    passed_groups = []
    for group in progress(groups):
        if group[0][0].lstrip("*") not in heads:
            passed_groups.append(group)
            continue
        removed_group, passed_group = [], []
        for cells in group:
            if cells[0].startswith("*"):
                removed_group.append(cells)
            else:
                passed_group.append(cells)
        if removed_group:
            removed_groups.append(removed_group)
        if passed_group:
            passed_groups.append(passed_group)
    removed_groups.sort(key=lambda g: g[0][0].lower())
    store_groups(removed_groups, filename="Звёздочки, вырезанные из групп.txt")
    return passed_groups


@stepper.step()
def step_6_3_cut_left_stars(groups, progress):
    for group in progress(groups):
        for cells in group:
            cells[0] = cells[0].lstrip("*")
    groups.sort(key=lambda g: g[0][0].lower())
    return groups


def save_groups(groups, *, filename, progress):
    write_file(groups_to_lines(groups), filename=filename, progress=progress)
    return groups


def lines_to_groups(lines):
    return list(gen_groups(lines))


stepper.step(filename="Рабочий-06.3.txt")(save_groups)


@stepper.step()
def strip_cells(groups, progress):
    new_groups = []
    for group in progress(groups):
        new_group = []
        for cells in group:
            new_group.append(list(map(str.strip, cells)))
        if new_group:
            new_groups.append(new_group)
    return new_groups


@stepper.step()
def hyphen_endings(groups, progress):
    s = "-де, -ка, -ко, -кось, -либо, -нибудь, -таки, -тка, -тко, -ткось, -то, -с"
    endings = tuple(map(str.strip, s.split(",")))
    words = []
    for group in progress(groups):
        if group[0][0].endswith(endings):
            words.append(group[0][0])
    write_file(words, filename="Список слов с частицами через дефис.txt")
    return groups


@stepper.step()
def step_7_1_hyphen_endings(groups, progress):
    s = "-либо, -нибудь, -то"
    endings = tuple(map(str.strip, s.split(",")))
    hyphen_groups = []
    for group in progress(groups):
        words = map(operator.itemgetter(0), group)
        if any(word.endswith(endings) for word in words):
            hyphen_groups.append(group)
    store_groups(hyphen_groups, filename="Слова с частицами.txt")
    return groups


@stepper.step()
def step_7_2_hyphen_endings(groups, progress):
    new_groups = []
    for group in progress(groups):
        new_group = []
        for cells in group:
            new_cells = []
            for cell in cells:
                if cell.endswith("-то"):
                    new_cells.append(cell[:-3])
                else:
                    new_cells.append(cell)
            new_group.append(new_cells)
        if new_group:
            new_groups.append(new_group)
    return new_groups


@stepper.step()
def step_7_3_sorting(groups, progress):
    groups.sort(key=lambda g: g[0][0].lower())
    return groups


def head_sort_key(h):
    return h.lower().replace("*", "")


def groups_sort_key(g):
    return head_sort_key(g[0][0])


@stepper.step(filename="Рабочий-07.2.txt")
def store_groups(groups, *, filename, progress=iter, groups_delimiter="\n"):
    size = collections.defaultdict(int)
    groups.sort(key=groups_sort_key)
    for group in groups:
        for words in group:
            for i, word in enumerate(words):
                size[i] = max(size[i], len(word))
    start = True
    with open(filename, "w", encoding="cp1251", newline="\r\n") as f:
        for group in progress(groups):
            if not start:
                f.write(groups_delimiter)
            start = False
            for words in group:
                formatted_words = []
                for i, w in enumerate(words):
                    if i == 0:
                        formatted_words.append(f"{w:<{size[i]}}")
                    else:
                        formatted_words.append(w)
                f.write(" .".join(formatted_words).strip())
                f.write("\n")
    return groups


def step_8_save_eqs(groups, eqs, filename):
    lines = []
    all_indexes = set()
    for ids in eqs:
        if len(ids) < 2:
            continue
        word = groups[next(iter(ids))][0][0]
        info = []
        for i in ids:
            info.append(groups[i][0][1].split()[0])
            all_indexes.add(i)
        lines.append(" ".join([word] + info))
    lines.sort()
    write_file(lines, filename=filename)
    return all_indexes


@stepper.step()
def step_8(groups, progress, step_number="8"):
    ordered_eqs = collections.defaultdict(set)
    unordered_eqs = collections.defaultdict(set)
    for i, group in progress(enumerate(groups)):
        if len(group[0]) < 2:
            continue
        words = tuple(map(operator.itemgetter(0), group))
        ordered_eqs[words].add(i)
        unordered_eqs[frozenset(words)].add(i)
    for words, ids in ordered_eqs.items():
        key = frozenset(words)
        if unordered_eqs[key] == ids:
            unordered_eqs.pop(key)
    ordered_indexes = step_8_save_eqs(
        groups,
        ordered_eqs.values(),
        f"{step_number}.1 Список ЗС абсолютно идентичных групп.txt",
    )
    unordered_indexes = step_8_save_eqs(
        groups,
        unordered_eqs.values(),
        f"{step_number}.2 Список ЗС идентичных групп.txt",
    )
    lines = set()
    for i in ordered_indexes & unordered_indexes:
        lines.add(groups[i][0][0])
    write_file(
        sorted(lines),
        filename=f"{step_number}.3 Список слов из {step_number}.1 и {step_number}.2.txt",
    )
    return groups


@stepper.step()
def step_8_extra(groups, progress):
    lines = []
    for group in progress(groups):
        for words in group:
            if len(words) < 2:
                lines.append(group[0][0])
                break
            elif len(words) > 2:
                raise Exception(repr(group))
    lines.sort()
    write_file(lines, filename="8.4 Список ЗС групп, в которых хотя бы 1 слово НЕ имеет спец. инфы.txt")


@stepper.step(filename="Рабочий-09.txt")
def load_groups(*_, filename, progress=lambda x: x):
    with open(filename, encoding="cp1251") as f:
        return list(gen_groups(progress(f), "."))


@stepper.step()
def step_9_1_remove_marked_groups(groups, progress):
    words = set(filter(len, read_file(filename="В КУНСТКАМЕРУ! .txt")))
    p = pathlib.Path("../10 - step_6_1_cut_left_stars/Кунсткамера.txt").resolve()
    new_groups = []
    special = load_groups(filename=str(p))
    for group in progress(groups):
        if group[0][0] not in words:
            new_groups.append(group)
        else:
            special.append(group)
    special.sort(key=groups_sort_key)
    store_groups(special, filename="Кунсткамера_9.txt")
    return new_groups


stepper.step(filename="Рабочий-09.2.txt")(store_groups)
stepper.step(step_number="9")(step_8)


@stepper.step()
def step_10_1_equal_first_words(groups, progress):
    first_words = collections.defaultdict(list)
    for group in progress(groups):
        first_words[group[0][0]].append(group[0][1].split()[0])
    lines = []
    for w in sorted(first_words):
        ws = first_words[w]
        if len(ws) > 1:
            lines.append(w + " " + " ".join(ws))
    write_file(lines, filename="10.1 одинаковые ЗС.txt")
    return groups


stepper.step(filename="Рабочий 10_1.txt")(load_groups)


@stepper.step()
def step_10_2_remove_marked(groups, progress):
    removed = load_groups(filename="Кунсткамера_9.txt")
    heads = set(read_file(filename="В КУНСТКАМЕРУ 10 .txt"))
    new_groups = []
    for group in progress(groups):
        if group[0][0] in heads:
            removed.append(group)
        else:
            new_groups.append(group)
    removed.sort(key=lambda g: g[0][0].lstrip("*").lower())
    store_groups(removed, filename="Кунсткамера_10.txt")
    return new_groups


@stepper.step()
def step_10_3_remove_marked(groups, progress):
    heads = set(read_file(filename="ВЫРЕЗАТЬ ЭТИ СУЩ-НЫЕ! .txt"))
    removed = []
    new_groups = []
    for group in progress(groups):
        if group[0][0] in heads and group[0][1].startswith("сущ"):
            removed.append(group)
        else:
            new_groups.append(group)
    store_groups(removed, filename="10.3 - Вырезанные существительные.txt")
    return new_groups


def filter_size_greater(d, size=1):
    for k, vs in d.items():
        if len(vs) > size:
            yield k, vs


@stepper.step()
def step_10_4_diff(groups, progress):
    equal = collections.defaultdict(set)
    for i, group in enumerate(groups):
        equal[group[0][0]].add(i)
    equal = dict(filter_size_greater(equal))
    extra_lines = []
    part_of_speech_lines = []
    verbs_lines = []
    missed_lines = []
    for head in progress(equal):
        gids = equal[head]
        extra = collections.defaultdict(set)
        part_of_speech = collections.defaultdict(set)
        verbs = collections.defaultdict(set)
        for i in gids:
            group = groups[i]
            first_line = group[0]
            if len(first_line) > 2:
                extra[first_line[2]].add(i)
            info = first_line[1].split()
            pos = info[0]
            part_of_speech[pos].add(i)
            if pos == "гл":
                verb_kind = info[1]
                if verb_kind != "2вид":
                    verbs[verb_kind].add(i)
        triggered = False
        if len(extra) > 1:
            extra_lines.append(head)
            triggered = True
        if len(part_of_speech) > 1:
            part_of_speech_lines.append(" ".join([head] + list(part_of_speech)))
            triggered = True
        if len(verbs) > 1:
            verbs_lines.append(head)
            triggered = True
        if not triggered:
            poss = map(lambda i: groups[i][0][1].split()[0], gids)
            missed_lines.append(" ".join([head] + list(poss)))
    write_file(extra_lines, filename="10.4.1 - одинаковые ЗС - разные примечания.txt")
    write_file(part_of_speech_lines, filename="10.4.2 - одинаковые ЗС - разные части речи.txt")
    write_file(verbs_lines, filename="10.4.3 - одинаковые глаголы - разный вид.txt")
    write_file(missed_lines, filename="10.4.4 - прочие случаи.txt")
    return groups


stepper.step(filename="Рабочий-10.4.txt")(store_groups)
stepper.step(filename="Рабочий-11.txt")(load_groups)


@stepper.step()
def step_11_generate_list_of_merge_groups(groups, progress):
    endings = ["ий", "ой", "ый", "ая", "яя", "ее", "ое", "ие", "ые"]
    endings.sort(key=len, reverse=True)
    parts = collections.defaultdict(set)
    for group in progress(groups):
        head = group[0][0]
        for end in endings:
            if head.endswith(end):
                parts[head[:-len(end)]].add(head)
                break
    parts = dict(filter_size_greater(parts))
    heads_lines = []
    for k in sorted(parts):
        heads = parts[k]
        heads_lines.append(" ".join(heads))
    write_file(heads_lines, filename="11.1 Список ЗС, отличающихся друг от друга окончаниями.txt")
    return groups


@stepper.step()
def step_11_2_strip_list_of_merge_groups(groups, progress):
    lines = read_file(filename="11.2 Список ЗС, отличающихся друг от друга окончаниями.txt")
    heads = list(map(str.split, lines))
    more_than_two = list(filter(lambda hs: len(hs) > 2, heads))
    if more_than_two:
        lines = list(map(" ".join, more_than_two))
        write_file(lines, filename="11.2 Более 2 слов в строке.txt")
        raise ValueError("More than 2 words per line")
    y_y, rest = [], []
    for hs in heads:
        assert len(hs) == 2
        if all(map(lambda w: w.endswith("й"), hs)):
            y_y.append(sorted(hs))
        else:
            rest.append(hs)
    lines = sorted(map(" ".join, y_y))
    write_file(lines, filename="11.2 Й - Й.txt")
    heads = rest
    for hs in heads:
        f, s = hs[0][-1], hs[1][-1]
        if f != s and s == "й":
            hs.reverse()
    lines = sorted(map(" ".join, heads))
    write_file(lines, filename="11.3 Список ЗС, отличающихся друг от друга окончаниями.txt")
    return groups


@stepper.step()
def step_11_3_check_full_absorb(groups, progress):
    words = collections.defaultdict(list)
    for group in progress(groups):
        words[group[0][0]].append(set(map(operator.itemgetter(0), group)))
    lines = read_file(filename="11.3 Список ЗС, отличающихся друг от друга окончаниями.txt")
    no_absorb = []
    for left, right in map(str.split, lines):
        if len(words[left]) != 1 or len(words[right]) != 1:
            raise Exception(f"{left} {right}")
        big = words[left][0]
        small = words[right][0]
        if not big >= small:
            no_absorb.append(" ".join([left, right]))
    if no_absorb:
        write_file(no_absorb, filename="11.3 Пары с неполным поглощением.txt")
    return groups


@stepper.step()
def step_11_4_remove_absorbable_groups(groups, progress):
    groups = load_groups(filename="Рабочий-11.4.txt")
    words = collections.defaultdict(list)
    for i, group in enumerate(progress(groups)):
        words[group[0][0]].append(i)
    lines = read_file(filename="11.4 Список ЗС, отличающихся друг от друга окончаниями.txt")
    no_absorb = []
    remove = []
    for left, right in map(str.split, lines):
        if len(words[left]) != 1 or len(words[right]) != 1:
            raise Exception(f"{left} {words[left]} {right} {words[right]}")
        big, small = map(lambda i: set(map(operator.itemgetter(0), groups[words[i][0]])), (left, right))
        if not big >= small:
            no_absorb.append(" ".join([left, right]))
        else:
            remove.append(words[right][0])
    remove.sort(reverse=True)
    removed = []
    for i in remove:
        removed.append(groups.pop(i))
    removed.reverse()
    store_groups(groups, filename="Рабочий-11.4-post.txt")
    store_groups(removed, filename="11.4 Поглощённое.txt")
    write_file(no_absorb, filename="11.4 Пары с неполным поглощением.txt")
    return groups


@stepper.step(filename="12.1 поглощение наречий.txt")
def step_12_1_list_groups_with_special_endings(groups, progress, filename):
    ENDINGS = ("ий", "ой", "ый", "е", "о")
    spec = collections.defaultdict(list)
    for g in progress(groups):
        head = g[0][0]
        for e in ENDINGS:
            if head.endswith(e):
                spec[head[:-len(e)]].append(head)
    lines = []
    strange_lines = []
    for key in sorted(spec):
        heads = spec[key]
        if len(heads) < 2:
            continue
        current_endings = []
        for h, e in itertools.product(heads, ENDINGS):
            if h.endswith(e):
                current_endings.append(e)
        lengths = sorted(map(len, current_endings))
        if lengths[0] != 1 or lengths[-1] != 2:
            strange_lines.append(" ".join(heads))
            continue
        heads.sort(key=lambda h: min(filter(lambda i: h.endswith(ENDINGS[i]), range(len(ENDINGS)))))
        lines.append(" ".join(heads))
    write_file(lines, filename=filename)
    write_file(strange_lines, filename="12.1 исключения.txt")
    return groups


@stepper.step()
def step_12_2_check_absorbtion(groups, progress):
    lines = read_file(filename="12.2 поглощение наречий.txt")
    bad_lines = []
    for line in progress(lines):
        if len(line.split()) > 2:
            bad_lines.append(line)
    write_file(bad_lines, filename="12.2 тройки.txt")
    return lines


@stepper.step()
def step_12_3_check_parts(lines, progress):
    groups = load_groups(filename="Рабочий-12.2.txt")
    parts = collections.defaultdict(list)
    for group in groups:
        parts[group[0][0]].append(group[0][1].split()[0])
    bad_lines = []
    for line in progress(lines):
        words = line.split()
        if len(words) != 2:
            raise RuntimeError(f"Not two words {repr(line)}")
        last = words[-1]
        if not last.endswith("о"):
            raise RuntimeError(f"Last words does not ends with 'o' {repr(line)}")
        current_parts = parts[last]
        if len(current_parts) != 1:
            raise RuntimeError(f"{repr(last)} not one head {repr(current_parts)}")
        if current_parts[0] != "нар":
            bad_lines.append(f"{last} {current_parts[0]}")
    write_file(bad_lines, filename="12.3 не наречия.txt")
    return groups, lines


@stepper.step()
def step_12_4_cut_parts(pack, progress):
    groups, lines = pack
    m = {}
    absorb_list = list(itertools.chain.from_iterable(map(str.split, lines)))
    absorb_set = set(absorb_list)
    assert len(absorb_list) == len(absorb_set)
    for i, g in enumerate(groups):
        head = g[0][0]
        if head in m and head in absorb_set:
            raise RuntimeError(f"Two groups with {head!r}")
        m[head] = i
    no_absorb = []
    absorb = []
    absorbed_lines = []
    for line in progress(lines):
        big_i, small_i = map(m.get, line.split())
        big, small = map(groups.__getitem__, (big_i, small_i))
        big_ws, small_ws = map(lambda g: {l[0] for l in g}, (big, small))
        if big_ws >= small_ws:
            absorb.append(small_i)
            absorbed_lines.append(line)
        else:
            no_absorb.append(line + " (" + " ".join(small_ws - big_ws) + ")")
    absorb.sort(reverse=True)
    absorbed_groups = []
    for i in absorb:
        absorbed_groups.append(groups.pop(i))
    absorbed_groups.reverse()
    write_file(absorbed_lines, filename="12.4 поглощаемые наречия.txt")
    write_file(no_absorb, filename="12.4 непоглощаемые наречия.txt")
    store_groups(absorbed_groups, filename="12.4 поглощённые наречия.txt")
    store_groups(groups, filename="Рабочий-12.4.txt")
    return groups


@stepper.step()
def step_12_5_partial_absorb(groups, progress):
    groups = load_groups(filename="Рабочий-12.5.txt")
    lines = read_file(filename="12.5 непоглощаемые наречия.txt")
    pairs = [line.split()[:2] for line in lines]
    heads = collections.defaultdict(list)
    for i, g in enumerate(groups):
        heads[g[0][0]].append(i)
    removed_groups = []
    absorbed_groups = []
    removed_i = []
    for pair in progress(pairs):
        for h in pair:
            if h not in heads:
                raise RuntimeError(f"{h!r} not in heads")
            if len(heads[h]) != 1:
                raise RuntimeError(f"{h!r} have multiple groups")
        ai, bi = (heads[h][0] for h in pair)
        a, b = groups[ai], groups[bi]
        words_a = {l[0] for l in a}
        remove = []
        absorb = []
        left = []
        for i, l in enumerate(b):
            if l[0] in words_a:
                remove.append(i)
            elif l[0][-1] in "ей":
                absorb.append(i)
            else:
                left.append(i)
        if not left:
            removed_i.append(bi)
        elif 0 not in left:
            raise RuntimeError(f"{a[0][0]!r} <- {b[0][0]!r} head absorbed")
        absorbed_groups.append([b[i] for i in absorb])
        removed_groups.append([b[i] for i in remove])
        for i in absorb:
            meta = b[i][1].split()
            if meta[0] != "нар":
                raise RuntimeError(f"{b[0][0]!r} -> {b!r} wrong part")
            meta[0] = "прл"
            changed = b[i][:]
            changed[1] = " ".join(meta)
            a.append(changed)
        groups[bi] = [b[i] for i in left]
    store_groups(removed_groups, filename="12.5 поглощённое (частично).txt")
    store_groups(absorbed_groups, filename="12.5 присоединённое.txt")
    removed_i.sort(reverse=True)
    for i in removed_i:
        groups.pop(i)
    store_groups(groups, filename="Рабочий-12.5-post.txt")
    return groups


stepper.step(filename="12.5 ЗС ий-ой-ый-е-о.txt")(step_12_1_list_groups_with_special_endings)


@stepper.step()
def step_12_6_remove_single(groups, progress):
    groups = load_groups(filename="Рабочий-12.6.txt")
    singles = load_groups(filename="Одиночки-12.6.txt")
    removed = []
    for i, g in enumerate(progress(groups)):
        if len(g) != 1:
            continue
        removed.append(i)
    removed.sort(reverse=True)
    for i in removed:
        singles.append(groups.pop(i))
    singles.sort(key=groups_sort_key)
    store_groups(singles, filename="Одиночки-12.6-post.txt")
    store_groups(groups, filename="Рабочий-12.6-post.txt")
    return groups


stepper.step(filename="12.6 ЗС ий-ой-ый-е-о.txt")(step_12_1_list_groups_with_special_endings)
stepper.step(filename="Рабочий-13.txt")(load_groups)


@stepper.step()
def step_13_1_create_lists(groups, progress):
    endings = (
        ("щий", "щийся", "щая", "щаяся", "щее", "щееся", "щие", "щиеся"),
        ("мый", "мая", "мое", "мые"),
        ("ший", "шийся", "шая", "шаяся", "шее", "шееся", "шие", "шиеся"),
        ("анный", "анная", "анное", "анные", "енный", "енная", "енное",
         "енные", "янный", "янная", "янное", "янные"),
        ("чатый", "чатая", "чатое", "чатые", "жатый", "жатая", "жатое",
         "жатые", "натый", "натая", "натое", "натые", "итый", "итая",
         "итое", "итые", "утый", "утая", "утое", "утые", "ртый", "ртая",
         "ртое", "ртые", "ятый", "ятая", "ятое", "ятые"),
    )
    lists = collections.defaultdict(list)
    for g in progress(groups):
        head = g[0][0]
        for i, es in enumerate(endings):
            if any(map(head.endswith, es)):
                lists[i].append(head)
    for i in range(len(endings)):
        write_file(lists[i], filename=f"13.1 список #{i + 1}.txt")
    return groups


def cut(groups, progress, *, heads, cutted, output):
    heads = set(read_file(filename=heads))
    cutted = load_groups(filename=cutted)
    new_groups = []
    for g in progress(groups):
        if g[0][0] in heads:
            cutted.append(g)
        else:
            new_groups.append(g)
    cutted.sort(key=lambda g: g[0][0].lstrip("*"))
    store_groups(cutted, filename=output)
    return new_groups


stepper.step(heads="В КУНСТКАМЕРУ 13.txt", cutted="Кунсткамера_10.txt", output="Кунсткамера_13_2.txt")(cut)
stepper.step(filename="Рабочий-13.2.txt")(store_groups)


@stepper.step()
def step_13_3_absorb(groups, progress):
    names = (
        "13.2 список #1.txt",
        "13.2 список #2.txt",
        "13.2 список #3.txt",
        "13.2 список #4a.txt",
        "13.2 список #4b.txt",
    )
    words_by_head = collections.defaultdict(list)
    for i, g in enumerate(groups):
        words_by_head[g[0][0]].append({l[0] for l in g})
    for name in progress(names):
        new_lines = []
        for line in read_file(filename=name):
            if not line:
                continue
            if len(words_by_head[line]) != 1:
                raise RuntimeError(f"Ambiguous head {line!r} and {words_by_head[line]!r}")
            partial, full = [], []
            absorbed_words = words_by_head[line][0]
            for head in words_by_head:
                if head == line:
                    continue
                for words in words_by_head[head]:
                    if words >= absorbed_words:
                        full.append(head)
                    elif words & absorbed_words:
                        partial.append((head, absorbed_words - words))
            if full:
                new_lines.append(" ".join([line] + full))
            elif partial:
                new_line = [f"*{line}"]
                for head, absorbed in partial:
                    new_line.append(f"{head} ({', '.join(absorbed)})")
                new_lines.append(" ".join(new_line))
            else:
                new_lines.append(f"!{line}")
        write_file(new_lines, filename=name.replace("13.2", "13.3"))
    return groups


@stepper.step()
def step_13_4_adj(groups, progress):
    words_by_head = collections.defaultdict(list)
    for i, g in enumerate(groups):
        words_by_head[g[0][0]].append({l[0] for l in g})
    new_lines = []
    for line in progress(read_file(filename="Прилагательные, ПОХОЖИЕ на причастия.txt")):
        if not line:
            continue
        if len(words_by_head[line]) != 1:
            raise RuntimeError(f"Ambiguous head {line!r} and {words_by_head[line]!r}")
        absorbed_words = words_by_head[line][0]
        absorb = []
        for head in words_by_head:
            if head == line:
                continue
            for words in words_by_head[head]:
                if line in words & absorbed_words:
                    absorb.append(head)
        if absorb:
            new_lines.append(" ".join([line] + absorb))
    write_file(new_lines, filename="13.4 прилагательные, ПОХОЖИЕ на причастия.txt")
    return groups


stepper.step(filename="Рабочий-13.4.txt")(load_groups)
stepper.step(heads="ЕЩЁ в кунсткамеру.txt", cutted="Кунсткамера_13_2.txt", output="Кунсткамера_13_5.txt")(cut)


@stepper.step()
def step_13_5_absorb(groups, progress):
    names = (
        "13.5 список #1.txt",
        "13.5 список #2.txt",
        "13.5 список #3.txt",
        "13.5 список #4b.txt",
    )
    groups_by_head = collections.defaultdict(list)
    for i, g in enumerate(groups):
        groups_by_head[g[0][0]].append(i)
    cutted_ids = []
    for name in progress(names):
        for line in map(str.strip, read_file(filename=name)):
            if not line:
                continue
            head_small, head_big = line.split()
            small_heads_count = len(groups_by_head[head_small])
            if not small_heads_count:
                continue
            if small_heads_count != 1:
                raise RuntimeError(f"{name} {head_small!r} -> {groups_by_head[head_small]}")
            id_small = groups_by_head[head_small][0]
            small = groups[id_small]
            small_words = {l[0] for l in small}
            for id_big in groups_by_head[head_big]:
                big = groups[id_big]
                big_words = {l[0] for l in big}
                if big_words >= small_words:
                    break
            else:
                raise RuntimeError(f"{name} {line}")
            cutted_ids.append(id_small)
    return groups, cutted_ids


def head_index(groups):
    groups_by_head = collections.defaultdict(list)
    for i, g in enumerate(groups):
        groups_by_head[g[0][0]].append(i)
    return groups_by_head


def words(group):
    return {l[0] for l in group}


@stepper.step()
def step_13_6(args, progress):
    groups, cutted_ids = args
    lines = read_file(filename="13.6. список #4a.txt")
    groups_by_head = head_index(groups)
    extra = []
    for line in progress(lines):
        small, big, *rest = line.strip().split()
        if len(groups_by_head[small.lstrip("*")]) != 1:
            raise RuntimeError(f"Not 1 group for {small} {groups_by_head[small]}")
        if small.startswith("*"):
            small = small.lstrip("*")
            i = groups_by_head[small][0]
            i_words = words(groups[i])
            for j in groups_by_head[big]:
                i_words -= words(groups[j])
            g = []
            for l in groups[i]:
                if l[0] in i_words:
                    g.append(l)
            if g:
                extra.append(g)
        i = groups_by_head[small][0]
        cutted_ids.append(i)
    cutted_ids.sort(reverse=True)
    absorbed_groups = []
    for i in cutted_ids:
        absorbed_groups.append(groups.pop(i))
    absorbed_groups.reverse()
    store_groups(absorbed_groups, filename="13.6 причастия, поглощённые глаголами.txt")
    store_groups(extra, filename="13.6 удалённое.txt")
    return groups


stepper.step(filename="Рабочий 13.6.txt")(store_groups)
stepper.step()(step_13_1_create_lists)
stepper.step()(step_13_3_absorb)


@stepper.step()
def step_13_extra(groups, progress):
    lines = read_file(filename="Какие слова ОБЩИЕ___ .txt")
    groups_by_head = head_index(groups)
    new_lines = []
    for line in progress(lines):
        a, b, *_ = line.lstrip("*").split()
        gs = []
        for x in (a, b):
            if len(groups_by_head[x]) != 1:
                raise RuntimeError(f"Not one group for head {x!r}")
            gs.append({l[0] for l in groups[groups_by_head[x][0]]})
        words = sorted(set.intersection(*gs))
        new_lines.append(f"*{a} {b} [{', '.join(words)}]")
    write_file(new_lines, filename="Список с общими словами.txt")
    return groups


@stepper.step()
def step_13_8_cut(groups, progress):
    groups = load_groups(filename="Рабочий-13.8.txt")
    lines = read_file(filename="Частичное поглощение.txt")
    groups_by_head = head_index(groups)
    removed_groups = []
    for line in map(str.strip, progress(lines)):
        if not line:
            continue
        for prefix in ("**", "*"):
            if not line.startswith(prefix):
                continue
            line = line[len(prefix):]
            head_absorbed, head_absorbing, *rest = line.split()
            common = set(" ".join(rest).strip("()").split(", "))
            groups_absorbed = groups_by_head[head_absorbed]
            groups_absorbing = groups_by_head[head_absorbing]
            pairs = []
            for i, j in itertools.product(groups_absorbed, groups_absorbing):
                if common == words(groups[i]) - words(groups[j]):
                    pairs.append((i, j))
            if len(pairs) != 1:
                raise RuntimeError(f"Not one pair {len(pairs)}")
            absorbed, absorbing = map(groups.__getitem__, pairs[0])
            absorbed[0][0] = prefix + head_absorbed
            words_absorbing = words(absorbing)
            removed_group = []
            removed = []
            for i, line in enumerate(absorbed):
                if i == 0:
                    removed_group.append(line)
                elif line[0] in words_absorbing and (prefix != "*" or "прл крат ед муж" not in line):
                    removed_group.append(line)
                    removed.append(i)
            removed_groups.append(removed_group)
            for i in reversed(removed):
                absorbed.pop(i)
            break
    store_groups(removed_groups, filename="Частично поглощённые причастия.txt")
    store_groups(groups, filename="Рабочий 13.8-post.txt")
    return groups


@stepper.step()
def step_14_pre(groups, progress):
    ones = load_groups(filename="Одиночки-14.txt")
    parts = set()
    by_head = collections.defaultdict(list)
    for g in progress(ones):
        try:
            head, meta, *_ = g[0]
        except ValueError:
            continue
        head = head.lstrip("*")
        part, *_ = meta.split()
        parts.add(part)
        by_head[head].append(part)
    write_file(sorted(parts), filename="14-pre все части речи.txt")
    write_file(sorted(" ".join([h] + ps) for h, ps in by_head.items() if len(ps) > 1), filename="14-pre повторы.txt")
    return groups


@stepper.step()
def step_14_1(groups, progress):
    ones = load_groups(filename="Одиночки-14.txt")
    lines = read_file(filename="Повторы.txt")
    by_head = collections.defaultdict(list)
    for i, g in enumerate(ones):
        by_head[g[0][0].lstrip("*")].append(i)
    remove = []
    removed = []
    for line in progress(lines):
        head, *rest = line.split()
        if head.startswith("*"):
            if any(w.startswith("*") for w in rest):
                raise RuntimeError(f"Bad line: {line!r}")
            head = head.lstrip("*")
            remove.extend(by_head[head])
            removed.extend(map(ones.__getitem__, by_head[head]))
        elif any(w.startswith("*") for w in rest):
            if len(rest) != len(by_head[head]):
                raise RuntimeError(f"Not equal groups count: {line!r} {by_head[head]!r}")
            parts = collections.defaultdict(list)
            for part in rest:
                if part.startswith("*"):
                    part = part.lstrip("*")
                    bucket = "save"
                else:
                    bucket = "remove"
                parts[bucket].append(part)
            for i in by_head[head]:
                g = ones[i]
                for p in parts["save"]:
                    if p in g[0][1]:
                        parts["save"].remove(p)
                        break
                else:
                    for p in parts["remove"]:
                        if p in g[0][1]:
                            remove.append(i)
                            parts["remove"].remove(p)
                            break
                    else:
                        raise RuntimeError(f"Not saved or removed: {g!r}")
            if parts["save"] or parts["remove"]:
                raise RuntimeError(f"Not all matched {parts!r}")
        else:
            raise RuntimeError(f"Not marked line: {line!r}")
    remove.sort(reverse=True)
    for i in remove:
        ones.pop(i)
    store_groups(removed, filename="14.1 удалённое из Одиночек.txt")
    store_groups(ones, filename="14.1 одиночки.txt")
    return groups


@stepper.step(fin="14.1 одиночки.txt", fout="14.2 список.txt")
def step_14_2(groups, progress, *, fin, fout):
    words = collections.defaultdict(list)
    for g in groups:
        for line in g:
            words[line[0].lstrip("*")].append(line[1])
    ones = load_groups(filename=fin)
    result = []
    for g in progress(ones):
        head = g[0][0].lstrip("*")
        if head not in words:
            continue
        result.append([[g[0][0]] + words[head]])
    store_groups(result, filename=fout, groups_delimiter="")
    return groups


@stepper.step()
def step_14_3(groups, progress):
    ones = load_groups(filename="14.2 Одиночки.txt")
    lines = read_file(filename="Удалить из Одиночек! .txt")
    index = head_index(ones)
    remove = []
    for line in progress(lines):
        head = line.split()[0].lstrip("*")
        if head not in index:
            raise RuntimeError(f"{head} no in index")
        if len(index[head]) > 1:
            raise RuntimeError(f"{head} have more than one index")
        remove.append(next(iter(index[head])))
    remove.sort(reverse=True)
    removed = []
    for i in remove:
        removed.append(ones.pop(i))
    removed.reverse()
    store_groups(removed, filename="Удалённое из Одиночек 2.txt")
    store_groups(ones, filename="14.3 одиночки.txt")
    return groups


stepper.step(filename="Рабочий ПОСЛЕДНЯЯ ВЕРСИЯ .txt")(load_groups)
stepper.step(fin="14.3 одиночки.txt", fout="14.4 список.txt")(step_14_2)


@stepper.step()
def step_14_4_part(groups, progress):
    gs = load_groups(filename="14.3 одиночки.txt")
    store = {
        "о": [],
        "е": [],
        "ки": [],
    }
    for g in progress(gs):
        for e in store:
            if g[0][0].endswith(e):
                store[e].append(g)
    for e, s in store.items():
        store_groups(s, filename=f"14.4 - -{e}.txt")
    return groups


stepper.step(filename="Рабочий 130418.txt")(load_groups)


@stepper.step()
def step_14_3_prepare(groups, progress):
    return groups, head_index(groups)


def _step_14_3_factory(args, progress, *, file_prefix, head_info, extenders):
    groups, index = args
    lines = read_file(filename=f"{file_prefix} .txt")
    missed = []
    for line in progress(lines):
        head = line[:head_info[0]] + head_info[1]
        indexes = index[head]
        if len(indexes) == 0:
            missed.append(line)
            continue
        elif len(indexes) > 1:
            raise RuntimeError(f"Expect 1 group for head {head!r}, got {len(indexes)}")
        group = groups[indexes[0]]
        for i, suffix, desc in extenders:
            group.append([line[:i] + suffix, desc])
    write_file(missed, filename=f"{file_prefix}-missed.txt")
    return groups, index


stepper.step(file_prefix="I 130418",
             head_info=(-1, "ый"),
             extenders=[
                 (-1, "", "прл крат ед муж"),
                 (-1, "а", "прл крат ед жен"),
                 (None, "", "прл крат ед ср"),
                 (-1, "ы", "прл крат мн"),
             ])(_step_14_3_factory)
stepper.step(file_prefix="II",
             head_info=(-1, "ый"),
             extenders=[
                 (-2, "", "прл крат ед муж"),
                 (-1, "а", "прл крат ед жен"),
                 (None, "", "прл крат ед ср"),
                 (-1, "ы", "прл крат мн"),
             ])(_step_14_3_factory)
stepper.step(file_prefix="III",
             head_info=(-1, "ый"),
             extenders=[
                 (-2, "ен", "прл крат ед муж"),
                 (-1, "а", "прл крат ед жен"),
                 (None, "", "прл крат ед ср"),
                 (-1, "ы", "прл крат мн"),
             ])(_step_14_3_factory)
stepper.step(file_prefix="IV",
             head_info=(-1, "ый"),
             extenders=[
                 (-3, "ен", "прл крат ед муж"),
                 (-1, "а", "прл крат ед жен"),
                 (None, "", "прл крат ед ср"),
                 (-1, "ы", "прл крат мн"),
             ])(_step_14_3_factory)
stepper.step(file_prefix="V",
             head_info=(-1, "ий"),
             extenders=[
                 (-3, "ек", "прл крат ед муж"),
                 (-1, "а", "прл крат ед жен"),
                 (None, "", "прл крат ед ср"),
                 (-1, "и", "прл крат мн"),
             ])(_step_14_3_factory)
stepper.step(file_prefix="V'",
             head_info=(-1, "ий"),
             extenders=[
                 (-1, "а", "прл крат ед жен"),
                 (None, "", "прл крат ед ср"),
                 (-1, "и", "прл крат мн"),
             ])(_step_14_3_factory)
stepper.step(file_prefix="V''",
             head_info=(-1, "ий"),
             extenders=[
                 (-1, "и", "прл крат мн"),
             ])(_step_14_3_factory)
stepper.step(file_prefix="VI",
             head_info=(-1, "ий"),
             extenders=[
                 (-2, "ок", "прл крат ед муж"),
                 (-1, "а", "прл крат ед жен"),
                 (None, "", "прл крат ед ср"),
                 (-1, "и", "прл крат мн"),
             ])(_step_14_3_factory)


@stepper.step()
def step_14_4(args, progress):
    groups = load_groups(filename="Одиночки 130418.txt")
    index_by_head = head_index(groups)
    remove = set()
    for prefix in progress(("I", "II", "III", "IV", "V", "V'", "V''", "VI")):
        for head in read_file(filename=f"{prefix} .txt"):
            indexes = index_by_head[head]
            if not indexes:
                continue
            if len(indexes) > 1:
                raise RuntimeError(f"{head} got {len(indexes)}")
            remove.add(indexes[0])
    for i in sorted(remove, reverse=True):
        groups.pop(i)
    store_groups(groups, filename="14.4 одиночки.txt")
    return args[0]


@stepper.step()
def step_14_5(groups, progress):
    removed = load_groups(filename="Кунсткамера_13_5.txt")
    index = head_index(groups)
    remove = set()
    for line in progress(read_file(filename="В Кунсткамеру 14.5.txt")):
        indexes = index[line]
        count = len(indexes)
        if count == 0:
            continue
        elif count > 1:
            raise RuntimeError(f"{line} {count}")
        remove.add(indexes[0])
    for i in sorted(remove, reverse=True):
        removed.append(groups.pop(i))
    store_groups(groups, filename="14.5 рабочий.txt")
    store_groups(removed, filename="14.5 кунсткамера.txt")
    return groups


@stepper.step()
def step_14_post(groups, progress):
    endings = ("отый", "отая", "отое", "отые", "ытый", "ытая", "ытое", "ытые")
    heads = []
    for g in groups:
        head = g[0][0]
        if head.endswith(endings):
            heads.append(head)
    write_file(heads, filename="4b+.txt")
    return groups


stepper.step(filename="Рабочий 170418.txt")(load_groups)


@stepper.step()
def step_15_pre(groups, progress):
    words = []
    watch = {"сущ", "мн"}
    for g in progress(groups):
        meta = set(g[0][1].split())
        if meta >= watch:
            words.append(g[0][0])
    write_file(words, filename="15-pre сущ мн.txt")
    return groups


stepper.step(filename="Рабочий 200418 .txt")(load_groups)


@stepper.step()
def step_15_1(groups, progress):
    lines = read_file(filename="В кунсткамеру 200418 .txt")
    index = head_index(groups)
    removed = load_groups(filename="14.5 кунсткамера.txt")
    remove = []
    for head in progress(lines):
        ids = index[head]
        if len(ids) != 1:
            raise RuntimeError(f"Not one head for {head!r}: {ids!r}")
        removed.append(groups[ids[0]])
        remove.append(ids[0])
    remove.reverse()
    for i in remove:
        groups.pop(i)
    store_groups(removed, filename="15.1 кунсткамера.txt")
    return groups


stepper.step(filename="15.1 рабочий.txt")(store_groups)


@stepper.step()
def step_15_1_post(groups, progress):
    lines = set(read_file(filename="Список 200418 .txt"))
    index = head_index(groups)
    present = sorted(lines & index.keys(), key=head_sort_key)
    write_file(present, filename="15.1-post список.txt")
    return groups


stepper.step(filename="Рабочий 300418.txt")(load_groups)


@stepper.step()
def step_15_x(groups, progress):
    heads = []
    for g in progress(groups):
        head, meta, *_ = g[0]
        meta = meta.strip().split()
        if "прл" not in meta:
            continue
        if any(v in meta for v in ("мн", "жен", "ср")):
            heads.append(head)
    write_file(heads, filename="15 прилагательные.txt")
    return groups


stepper.step(filename="Рабочий 060518.txt")(load_groups)


@stepper.step()
def step_15_x(groups, progress):
    lists = collections.defaultdict(list)
    for g in progress(groups):
        head, meta, *_ = g[0]
        meta = set(meta.split())
        if head.endswith(("ейший", "ейшая", "ейшее", "ейшие", "айший", "айшая", "айшее", "айшие")):
            lists[3].append(head)
        if "сущ" not in meta:
            continue
        if head.endswith(("ая", "ее", "ое", "яя")):
            lists[1].append(head)
        if {"ед", "муж", "им"} <= meta and head.endswith(("ий", "ой", "ый")):
            for sub_head, sub_meta, *_ in g:
                sub_meta = set(sub_meta.split())
                if {"ед", "муж", "род"} <= sub_meta and sub_head.endswith(("его", "ого")):
                    lists[2].append(head)
                    break
    for i, lines in lists.items():
        write_file(lines, filename=f"{i}.txt")


stepper.step(filename="Рабочий 180518.txt")(load_groups)


@stepper.step()
def step_15_2(groups, progress):
    heads = read_file(filename="Перевести в прилагательные.txt")
    index = head_index(groups)
    for head in progress(heads):
        if len(index[head]) != 1:
            raise RuntimeError(f"Not one head in index for {head} ({index[head]})")
        g = groups[index[head][0]]
        for line in g:
            line[1] = line[1].replace("сущ неод", "прл").replace("сущ одуш", "прл")
    return groups


stepper.step(filename="Рабочий 180525.txt")(store_groups)


@stepper.step()
def step_15_2_post(groups, progress):
    heads = []
    for g in progress(groups):
        words = {l[0] for l in g}
        if len(words) == 1:
            heads.append(next(iter(words)))
    write_file(heads, filename="группы-слова.txt")


stepper.step(filename="Рабочий 250518.txt")(load_groups)


@stepper.step()
def step_15_3(groups, progress):
    ones = load_groups(filename="Одиночки 250518.txt")
    heads = read_file(filename="Добавить в Одиночки.txt")
    index = head_index(groups)
    remove = []
    for head in progress(heads):
        if len(index[head]) != 1:
            raise RuntimeError(f"Not one head in index for {head} ({index[head]})")
        remove.append(next(iter(index[head])))
    remove.reverse()
    for i in remove:
        ones.append(groups.pop(i)[:1])
    store_groups(ones, filename="Одиночки 250518-after.txt")
    write_file({g[0][1].split()[0] for g in ones}, filename="Части речи одиночки.txt")
    write_file({g[0][1].split()[0] for g in groups}, filename="Части речи рабочий.txt")
    return groups


stepper.step(filename="Рабочий 250518-after.txt")(store_groups)


@stepper.step()
def step_16(groups, progress):
    heads = collections.defaultdict(list)
    for g in progress(groups):
        parts = [l[1].split()[0] for l in g]
        for part in ("числ", "мест", "нар"):
            if part in parts:
                heads[part].append(g[0][0])
        if "гл" != parts[0] and len(set(parts)) > 1:
            heads["multi"].append(f"{g[0][0]} ({', '.join(set(parts))})")
    for k, lines in heads.items():
        write_file(lines, filename=f"Части речи {k}.txt")


stepper.step(filename="Одиночки 250518-after.txt")(load_groups)


@stepper.step()
def step_14_6(groups, progress):
    m = collections.defaultdict(list)
    for g in progress(groups):
        m[g[0][1].split()[0]].append(g)
    for k, gs in m.items():
        store_groups(gs, filename=f"одиночки ({k}).txt")


stepper.step(filename="Рабочий 300518.txt")(load_groups)


@stepper.step()
def step_16_1(groups, progress):
    lines = read_file(filename="Числительные в прилагательные.txt")
    index = head_index(groups)
    for head in progress(lines):
        if len(index[head]) != 1:
            raise RuntimeError(f"Not one head for {head!r} ({index[head]!r}")
        i, *_ = index[head]
        for line in groups[i]:
            s = "числ поряд"
            if s not in line[1] and "прл" not in line[1]:
                print(head, line)
                # raise RuntimeError(f"{s!r} not in line {line!r}")
            line[1] = line[1].replace(s, "прл")
    return groups


@stepper.step()
def step_16_2(groups, progress):
    lines = read_file(filename="Местоимения в прилагательные.txt")
    index = head_index(groups)
    for head in progress(lines):
        if not head:
            continue
        if len(index[head]) != 1:
            raise RuntimeError(f"Not one head for {head!r} ({index[head]!r}")
        i, *_ = index[head]
        for line in groups[i]:
            s = "мест прил"
            if s not in line[1] and "прл" not in line[1]:
                print(head, line)
                # raise RuntimeError(f"{s!r} not in line {line!r}")
            line[1] = line[1].replace(s, "прл")
    return groups


stepper.step(filename="Рабочий 06.06.18.txt")(store_groups)


@stepper.step()
def check_work_for_multiwords(groups, progress):
    heads = []
    for g in progress(groups):
        for line in g:
            if " " in line[0]:
                heads.append(g[0][0])
                break
    write_file(heads, filename="multiwords.txt")


stepper.step(filename="Одиночки 020618.txt")(load_groups)


@stepper.step()
def step_14_7_strip_groups_to_lines(groups, progress):
    heads = [g[0][0] for g in progress(groups)]
    write_file(heads, filename="Одиночки 09.06.18.txt")
    return heads


@stepper.step()
def step_14_8(lines, progress):
    heads = set(lines)
    write_file(sorted(heads & set(read_file(filename="Список №1.txt"))), filename="Список 1.txt")
    write_file(sorted(heads & set(read_file(filename="Список №2.txt"))), filename="Список 2.txt")
    third = []
    for end in progress(read_file(filename="Список №3.txt")):
        for l in lines:
            if l.endswith(end):
                third.append(l)
    write_file(third, filename="Список 3.txt")


stepper.step(filename="Рабочий 100618.txt")(load_groups)


@stepper.step()
def step_17_1_17_2(groups, progress):
    parts = {
        "гл": ("Г", "Глаголы"),
        "прч": ("П", "Глаголы"),
        "дееп": ("Д", "Глаголы"),
        "мест": ("М", "Местоимения"),
        "нар": ("Н", "Наречия"),
        "прл": ("П", "Прилагательные"),
        "сущ": ("С", "Существительные"),
        "числ": ("Ч", "Числительные"),
    }
    m = collections.defaultdict(list)
    for g in progress(groups):
        global_target = None
        for line in g:
            part, *rest = line[1].split()
            if part not in parts:
                print(" ".join(line))
                break
                # raise RuntimeError(f"Unknown part {line!r}")
            rep, target = parts[part]
            if global_target is None:
                global_target = target
            line[1] = " ".join([rep] + rest)
        m[global_target].append(g)
    for name, gs in m.items():
        store_groups(gs, filename=f"{name}.txt")


stepper.step(filename="Глаголы.txt")(load_groups)


@stepper.step()
def step_17_3(groups, progress):
    for g in progress(groups):
        for line in g:
            meta = line[1].split()
            new_meta = []
            for info in meta:
                if info == "воз":
                    continue
                if info == "инф":
                    new_meta[0] = "ГИ"
                    continue
                new_meta.append(info)
            line[1] = " ".join(new_meta)
    return groups


@stepper.step()
def step_17_4(groups, progress):
    changes = [
        ("ГНБ", {"наст", "Г"}),
        ("ГНБ", {"буд", "Г"}),
        ("ГП", {"прош", "Г"}),
        ("ГПв", {"пов", "Г"}),
        ("ПНС", {"П", "страд", "наст"}),
        ("ПНД", {"П", "наст"}),
        ("ППС", {"П", "страд", "прош"}),
        ("ППД", {"П", "прош"}),
    ]
    for g in progress(groups):
        for line in g:
            meta = line[1].split()
            meta_set = set(meta)
            for rep, s in changes:
                if s <= meta_set:
                    for item in s:
                        meta.remove(item)
                    meta.insert(0, rep)
                    break
            line[1] = " ".join(meta)
    store_groups(groups, filename="Глаголы 11.06.18.txt")


@stepper.step()
def step_17_5(*_, progress):
    groups = load_groups(filename="Местоимения.txt")
    for g in progress(groups):
        for line in g:
            meta = line[1].split()
            for v in ("прил", "сущ"):
                with contextlib.suppress(ValueError):
                    meta.remove(v)
            line[1] = " ".join(meta)
    store_groups(groups, filename="Местоимения 11.06.18.txt")


@stepper.step()
def step_17_6(*_, progress):
    changes = [
        ("ПК", {"крат", "П"}),
        ("ПС", {"сравн", "П"}),
        ("ПП", {"прев", "П"}),
    ]
    groups = load_groups(filename="Прилагательные.txt")
    for g in progress(groups):
        for line in g:
            meta = line[1].split()
            meta_set = set(meta)
            for rep, s in changes:
                if s <= meta_set:
                    for item in s:
                        meta.remove(item)
                    meta.insert(0, rep)
                    break
            line[1] = " ".join(meta)
    store_groups(groups, filename="Прилагательные 11.06.18.txt")


@stepper.step()
def step_17_7(*_, progress):
    groups = load_groups(filename="Числительные.txt")
    for g in progress(groups):
        for line in g:
            meta = line[1].split()
            for v in ("кол", "собир"):
                with contextlib.suppress(ValueError):
                    meta.remove(v)
            line[1] = " ".join(meta)
    store_groups(groups, filename="Числительные 11.06.18.txt")


stepper.step(filename="Глаголы 11.06.18.txt")(load_groups)


@stepper.step()
def step_17_4_more(groups, progress):
    changes = [
        ("ПНСК", {"ПНС", "крат"}),
        ("ПНДК", {"ПНД", "крат"}),
        ("ППСК", {"ППС", "крат"}),
        ("ППДК", {"ППД", "крат"}),
    ]
    for g in progress(groups):
        for line in g:
            meta = line[1].split()
            meta_set = set(meta)
            for rep, s in changes:
                if s <= meta_set:
                    for item in s:
                        meta.remove(item)
                    meta.insert(0, rep)
                    break
            line[1] = " ".join(meta)
    store_groups(groups, filename="Глаголы 15.06.18.txt")


@stepper.step()
def step_17_5_more(*_, progress):
    groups = load_groups(filename="Местоимения 11.06.18.txt")
    rules = [json.loads(pathlib.Path(n).read_text()) for n in ("местоимения1.json", "местоимения2.json")]
    for g in progress(groups):
        head = g[0][0]
        for rule in rules:
            if head in rule["targets"]:
                for line in g:
                    line[1] = rule["rename"].get(line[1], line[1])
    store_groups(groups, filename="Местоимения 15.06.18.txt")


@stepper.step()
def step_17_6_more(*_, progress):
    groups = load_groups(filename="Прилагательные 11.06.18.txt")
    rule = json.loads(pathlib.Path("прилагательные.json").read_text())
    for g in progress(groups):
        for line in g:
            line[1] = rule.get(line[1], line[1])
    store_groups(groups, filename="Прилагательные 15.06.18.txt")


@stepper.step()
def step_17_7_more(*_, progress):
    groups = load_groups(filename="Числительные 11.06.18.txt")
    rules = [json.loads(pathlib.Path(n).read_text()) for n in ("числительные1.json", "числительные2.json")]
    for g in progress(groups):
        head = g[0][0]
        if head in rules[0]["targets"]:
            for line in g:
                line[1] = rules[0]["rename"].get(line[1], line[1])
        else:
            for line in g:
                line[1] = rules[1].get(line[1], line[1])
    store_groups(groups, filename="Числительные 15.06.18.txt")


@stepper.step()
def step_17_8_pre(*_, progress):
    groups = load_groups(filename="Существительные.txt")
    both = []
    for g in groups:
        meta = set()
        for line in g:
            head, meta_s, *_ = g[0]
            meta |= set(meta_s.split())
        if meta >= {"неод", "одуш"}:
            both.append(head)
    write_file(both, filename="одуш-неод.txt")


def read_csv(name):
    with pathlib.Path(name).open() as f:
        return list(csv.reader(f))


@stepper.step()
def step_17_4_more(*_, progress):
    groups = load_groups(filename="Глаголы 15.06.18.txt")
    rep = {l.lstrip("."): r.lstrip(".") for l, r in read_csv("Глаголы.csv")}
    for g in progress(groups):
        for line in g:
            meta = line[1].split()
            for v in ("сов", "несов", "2вид",  "перех", "непер", "пер/не"):
                if v in meta:
                    meta.remove(v)
                    meta.append(v)
            new_line = " ".join(meta)
            for key, replace in rep.items():
                if new_line.startswith(key):
                    new_line = new_line.replace(key, replace)
                    break
            line[1] = new_line
    store_groups(groups, filename="Глаголы 04.07.18.txt")


def meta_repeat_factory(groups, progress, source, target):
    if source is not None:
        groups = load_groups(filename=source)
    repeats = set()
    for g in progress(groups):
        c = collections.Counter(line[1] for line in g)
        for meta, count in c.most_common():
            if count > 1:
                repeats.add(meta)
    write_file(sorted(repeats), filename=target)


stepper.step(source="Местоимения 16.06.18.txt",
             target="Местоимения (повторения) 04.07.18.txt")(meta_repeat_factory)
stepper.step(source="Прилагательные 15.06.18.txt",
             target="Прилагательные (повторения) 04.07.18.txt")(meta_repeat_factory)
stepper.step(source="Числительные 16.06.18.txt",
             target="Числительные (повторения) 04.07.18.txt")(meta_repeat_factory)


@stepper.step()
def step_17_8(*_, progress):
    groups = load_groups(filename="Существительные.txt")
    rep = {l.lstrip("."): r.lstrip(".") for l, r in read_csv("Существительные.csv")}
    for g in progress(groups):
        for i, line in enumerate(g):
            meta = line[1].split()
            for v in ("неод", "одуш"):
                if v in meta:
                    meta.remove(v)
                    if i == 0:
                        meta.append(v)
            new_line = " ".join(meta)
            for key, replace in rep.items():
                if new_line.startswith(key):
                    new_line = new_line.replace(key, replace)
                    break
            line[1] = new_line
    store_groups(groups, filename="Существительные 04.07.18.txt")


@stepper.step()
def step_17_4_more(*_, progress):
    groups = load_groups(filename="Глаголы 04.07.18.txt")
    rep = {l.lstrip("."): r.lstrip(".") for l, r in read_csv("Глаголы (продолжение).csv")}
    extra = {"2в": [], "пер/не": []}
    for g in progress(groups):
        for line in g:
            meta = line[1].split()
            if meta[0] == "Гпве":
                meta[0] = "ГПве"
            meta_line = " ".join(meta)
            for l, r in rep.items():
                meta_line = meta_line.replace(l, r)
            line[1] = meta_line
        meta = g[0][1].split()
        for k, v in extra.items():
            if k in meta:
                extra[k].append(g[0][0])
    for k, v in extra.items():
        write_file(v, filename=f"{k} 05.07.18.txt".replace("/", ""))
    return groups


stepper.step(filename="Глаголы 05.07.18.txt")(store_groups)
stepper.step(source=None, target="Глаголы (повторения) 05.07.18.txt")(meta_repeat_factory)
stepper.step(filename="Существительные 04.07.18.txt")(load_groups)


@stepper.step()
def step_17_8_more(groups, progress):
    extra = []
    for g in progress(groups):
        meta = set(g[0][1].split())
        if not {"неод", "одуш"} & meta:
            extra.append(g[0][0])
    write_file(extra, filename="Существительные (зс без одуш, неод) 05.07.18.txt")
    return groups


stepper.step(source=None, target="Существительные (повторения) 05.07.18.txt")(meta_repeat_factory)
stepper.step(filename="Существительные 06.07.18.txt")(load_groups)


@stepper.step()
def step_17_8_4(groups, progress):
    heads = read_file(filename="Список существительных.txt")
    index = head_index(groups)
    kunst = load_groups(filename="15.1 кунсткамера.txt")
    remove = []
    for head in progress(heads):
        if head.startswith("!"):
            head = head.lstrip("!")
            if len(index[head]) != 1:
                raise ValueError(f"Got {len(index[head])} for head {head} instead of 1")
            remove.append(index[head][0])
        else:
            parts = head.split()
            if len(parts) < 2:
                continue
            head, key = parts
            if len(index[head]) != 1:
                raise ValueError(f"Got {len(index[head])} for head {head} instead of 1")
            i, *_ = index[head]
            g = groups[i]
            if key == "о":
                g[0][1] += " одуш"
            elif key == "н":
                g[0][1] += " неод"
            else:
                raise ValueError(f"Unknown key {key}")
    remove.sort(reverse=True)
    for i in remove:
        kunst.append(groups.pop(i))
    store_groups(kunst, filename="18.4 кунсткамера.txt")
    return groups


@stepper.step()
def step_17_8_6(groups, progress):
    rules = (
        ("С ед общ им", "СоИ"),
        ("С ед общ род", "СоР"),
        ("С ед общ дат", "СоД"),
        ("С ед общ вин", "СоВ"),
        ("С ед общ тв", "СоТ"),
        ("С ед общ пр", "СоП"),
    )
    for g in progress(groups):
        for line in g:
            for k, v in rules:
                if k in line[1]:
                    line[1] = line[1].replace(k, v)
                    break
    return groups


stepper.step(filename="Существительные 18.07.18.txt")(store_groups)


@stepper.step()
def step_17_8_6_post(groups, progress):
    heads = []
    for g in progress(groups):
        if "СоИ" in g[0][1]:
            heads.append(g[0][0])
    write_file(heads, filename="Список существительных с СоИ.txt")
    return groups


stepper.step(filename="Глаголы 05.07.18.txt")(load_groups)


@stepper.step()
def step_17_4_18(groups, progress):
    for g in progress(groups):
        for line in g:
            line[1] = line[1].replace("пер/не", "пер")
    return groups


stepper.step(filename="Глаголы 18.07.18.txt")(store_groups)


@stepper.step()
def step_17_4_19(groups, progress):
    groups = load_groups(filename="Глаголы 21.07.18.txt")
    d_only = []
    all_parts = set()
    bezl = []
    for g in progress(groups):
        for line in g:
            head, meta, *_ = line
            if "Д нес" in meta:
                if head.endswith(("а", "я", "ась", "ясь")):
                    line[1] = meta.replace("Д нес", "ДН нес")
                elif head.endswith(("в", "ши", "шись")):
                    line[1] = meta.replace("Д нес", "ДП нес")
            elif "Д сов" in meta:
                if head.endswith(("а", "я", "ась", "ясь")):
                    line[1] = meta.replace("Д сов", "ДП сов")
                elif head.endswith(("в", "ши", "шись")):
                    line[1] = meta.replace("Д сов", "ДП сов")
            parts = line[1].split()
            if "Д" in parts:
                d_only.append(head)
            all_parts |= set(parts[1:])
        if "безл" in g[0][1]:
            bezl.append(g[0][0])
    write_file(d_only, filename="17.4.21 - только Д.txt")
    all_parts -= {"сов", "нес", "2в", "пер", "неп"}
    write_file(all_parts, filename="17.4.21 - метаинформация.txt")
    write_file(bezl, filename="17.4.21 - ЗС с безл.txt")
    return groups


@stepper.step()
def step_17_4_21_1(groups, progress):
    w1 = []
    w2 = []
    repeats = set()
    for g in progress(groups):
        all_parts = set()
        first = set()
        triggered = False
        for line in g:
            head, meta, *_ = line
            meta = meta.split()
            meta_set = set(meta)
            if {"сов", "нес"} & meta_set and meta[0] not in ("Д", "ДН", "ДП"):
                triggered = True
            all_parts |= meta_set
            if meta[0] in first:
                repeats.add(meta[0])
            first.add(meta[0])
        i = all_parts & {"сов", "нес", "2в"}
        if len(i) != 1 and triggered:
            w1.append(g[0][0])
        i = all_parts & {"пер", "неп"}
        if len(i) != 1:
            w2.append(g[0][0])
    write_file(w1, filename="17.4.21 - зс с отличиями сов-нес-2в.txt")
    write_file(w2, filename="17.4.21 - зс с отличиями пер-неп.txt")
    write_file(sorted(repeats), filename="17.4.21 - повторения идентификатора в группе.txt")
    store_groups(groups, filename="Глаголы 31.07.18.txt")
    return groups


stepper.step(filename="Существительные 20.07.18.txt")(load_groups)


@stepper.step()
def step_17_8_7(groups, progress):
    data = {
        "Со в См.txt": {
            "СоИ": "СмИ",
            "СоР": "СмР",
            "СоД": "СмД",
            "СоВ": "СмВ",
            "СоТ": "СмТ",
            "СоП": "СмП",
        },
        "Со в Сж.txt": {
            "СоИ": "СжИ",
            "СоР": "СжР",
            "СоД": "СжД",
            "СоВ": "СжВ",
            "СоТ": "СжТ",
            "СоП": "СжП",
        },
    }
    index = head_index(groups)
    for name, rules in data.items():
        for head in read_file(filename=name):
            ids = index[head]
            if len(ids) != 1:
                raise RuntimeError(f"{head} -> {ids}")
            g = groups[ids[0]]
            for l, r in rules.items():
                g[0][1] = g[0][1].replace(l, r)
    return groups


stepper.step(filename="Существительные 31.07.18.txt")(store_groups)


if __name__ == "__main__":
    stepper.run(unchanged=True, start_index=80)
